#include <stdio.h>
#include <sys/types.h>

int main(void) {

	pid_t pid;
	pid = fork();

	if (pid == -1) { 
		fprintf(stderr,"fork failed\n");
		exit(1);
	}

	// Ovoj kod go izvrsuva procesot-dete
	if (pid == 0) { 
		printf("I'm the child\n");
		exit(0);
	}

	// Ovoj kod go izvrsuva procesot-tatko
	if (pid > 0) { 
		printf("I'm the parent with child %d\n", pid);
		exit(0);
	}
}